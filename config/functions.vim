" Remove trailing whitespace function
function! g:StripTrailingWhitespace() abort
  if !&binary && &filetype != 'diff'
    normal mz
    normal Hmy
    %s/\s\+$//e
    normal 'yz<CR>
    normal `z
  endif
  echon 'Whitespace stripped'
endfunction

" Open current file with app given funtion
function! g:OpenWith(appname) abort
  noautocmd silent execute '!open -a "' . a:appname . '" "%"'
  if v:shell_error
    echohl Error
    echon 'Problem opening the file'
    echohl Normal
  endif
endfunction
command! -bar -nargs=1 OpenWith call OpenWith(<f-args>)

" Coc check back space
function! g:CheckBackSpace() abort
  let col=col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Set to space indent width 2
function! g:Ispaces() abort
  set expandtab
  set smarttab
  set shiftwidth=2
  set softtabstop=2
  set tabstop=2
  echo "Space Indentation"
endfunction

" Set to tab indent width 2
function! s:Itabs() abort
  set noexpandtab
  set nosmarttab
  set shiftwidth=2
  set softtabstop=2
  set tabstop=2
  echo "Tab Indentation"
endfunction
