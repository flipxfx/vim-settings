" JSX highlighting on Javascript files
let g:jsx_ext_required=0

" Javascript libraries
let g:used_javascript_libs='react'

" Markdown list without auto-indent
let g:vim_markdown_new_list_item_indent=0

" Markdown preview don't close
let g:mkdp_auto_close=0

" Remove indent guides from filetypes
let g:indent_guides_exclude_filetypes=['help', 'coc-explorer']

" DelimitMate auto-close, jump, space, character return and inside quotes
let g:delimitMate_autoclose=1
let g:delimitMate_jump_expansion=1
let g:delimitMate_expand_space=1
let g:delimitMate_expand_cr=2
let g:delimitMate_expand_inside_quotes=1
let g:delimitMate_balance_matchpairs=0

" Coc extensions
let g:coc_global_extensions=[
  \ 'coc-css', 
  \ 'coc-emoji', 
  \ 'coc-explorer',
  \ 'coc-git',
  \ 'coc-highlight', 
  \ 'coc-html',
  \ 'coc-jest', 
  \ 'coc-json', 
  \ 'coc-lists', 
  \ 'coc-sh',
  \ 'coc-snippets', 
  \ 'coc-tsserver',
  \ 'coc-vetur',
  \ 'coc-tailwindcss'
\ ]

" Coc airline integration
let g:airline_section_error='%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning='%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'

" Coc snippet settings
let g:coc_snippet_next = '<tab>'

" Coc preferences
call coc#config('preferences', {
  \ 'extensionUpdateCheck': 'weekly'
\ })

" Coc suggest settings
call coc#config('suggest', {
  \ 'disableKind': 1,
  \ 'disableMenu': 1,
  \ 'maxCompleteItemCount': 10
\ })

" Coc diagnostic settings
call coc#config('diagnostic', {
  \ 'enableSign': 1
\ })

" Coc list source settings
let list_ignore='!**/{.git,package-lock.json,yarn.lock,Gemfile.lock}'
call coc#config('list.source', {
  \ 'files.args': ['--color', 'never', '--files', '--hidden', '--glob', list_ignore],
  \ 'grep.args': ['--smart-case', '--hidden', '--glob', list_ignore],
  \ 'grep.useLiteral': 0
\ })

" Coc buffer source settings
call coc#config('coc.source.buffer', {
  \ 'ignoreGitignore': 0
\ })

" Coc explorer settings
call coc#config('explorer', {
  \ 'icon.enableNerdfont': 1,
  \ 'icon.source': 'vim-devicons',
  \ 'explorer.sources': [
    \ { 'name': 'file', 'expand': 1 },
    \ { 'name': 'buffer', 'expand': 1 }
  \ ],
  \ 'git.enable': 0,
  \ 'filename.colored.enable': 0,
  \ 'file.column.indent.indentLine': 0,
  \ 'openAction.strategy': 'select',
  \ 'file.child.template': '[selection | clip | 1] [indent][icon | 1] [diagnosticError & 1][filename omitCenter 1][modified][readonly] [linkIcon & 1][link growRight 1 omitCenter 5][size]',
  \ 'buffer.child.template': '[selection | 1] [bufnr] [name][modified][readonly] [fullpath]'
\ })

" Coc git settings
call coc#config('git', {
  \ 'enableGutters': 0,
  \ 'addedSign.text': '┃',
  \ 'changedSign.text': '┃',
  \ 'removedSign.text': '◢',
  \ 'topRemovedSign.text': '◥',
  \ 'changeRemovedSign.text': '◢',
  \ 'addedSign.hlGroup': 'GitGutterAdd',
  \ 'changedSign.hlGroup': 'GitGutterDelete',
  \ 'removedSign.hlGroup': 'GitGutterDelete',
  \ 'topRemovedSign.hlGroup': 'GitGutterDelete',
  \ 'changeRemovedSign.hlGroup': 'GitGutterChangeDelete'
\ })

" Coc javascript settings
call coc#config('javascript', {
  \ 'format.insertSpaceBeforeFunctionParenthesis': 1,
  \ 'preferences.noSemicolons': 1,
  \ 'suggest.paths': 0,
  \ 'suggestionActions.enabled': 0
\ })

" Coc emoji source settings
call coc#config('coc.source.emoji', {
  \ 'filetypes': ['markdown', 'gitcommit']
\ })

" NERDCommenter use space after delimiters
let g:NERDSpaceDelims=1

" NERDCommenter alternate Javascript comment for JSX
let g:NERDCustomDelimiters={
  \ 'javascript': { 'left': '//', 'right': '', 'leftAlt': '{/*', 'rightAlt': '*/}' }
\ }

" Airline auto-populate symbols
let g:airline_powerline_fonts=1

" Airline smarter tab line
let g:airline#extensions#tabline#enabled=1

" Airline only show filename in tab line
let g:airline#extensions#tabline#fnamemod=':t'

" Airline change section z
let g:airline_section_z='%4l/%L:%3v'
let g:airline#extensions#wordcount#enabled=0

" Indent guides enable
let g:indent_guides_enable_on_vim_startup=1

" Indent guides size
let g:indent_guides_guide_size=1

" Enable display colors for filetypes
let g:Hexokinase_ftAutoload=['css', 'scss']

" Sayonara behavior on certain file types
let g:sayonara_filetypes={ 'coc-explorer': 'CocCommand explorer' }
