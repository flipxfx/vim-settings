" Airline tab bar
Plug 'vim-airline/vim-airline'

" Automatic commenting
Plug 'scrooloose/nerdcommenter'

" Coc autocompletion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Git wrapper
Plug 'tpope/vim-fugitive'

" Mappings to manipulate surroundings
Plug 'tpope/vim-surround'

" Live replace previews
Plug 'osyo-manga/vim-over', { 'on': 'OverCommandLine' }

" Easy tables
Plug 'godlygeek/tabular', { 'for': 'markdown' }

" Delete buffers except current
Plug 'schickling/vim-bufonly', { 'on': 'BufOnly' }

" Diff directories
Plug 'will133/vim-dirdiff'

" Better gf for node
Plug 'tomarrell/vim-npr'

" Auto-close
Plug 'Raimondi/delimitMate'

" Indent guides
Plug 'nathanaelkane/vim-indent-guides'

" Easier buffer deletion
Plug 'mhinz/vim-sayonara', { 'on': 'Sayonara' }

" Styled JSX
Plug 'alampros/vim-styled-jsx'

" Languages
Plug 'sheerun/vim-polyglot'

" Automatically detect indent for file
Plug 'tpope/vim-sleuth'

" Markdown preview
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

" Gruvbox
Plug 'morhetz/gruvbox'

" Devicons for tabs/buffers etc.
Plug 'ryanoasis/vim-devicons'

" Call plug end
call plug#end()
